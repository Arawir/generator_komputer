#ifndef SERIALPORTINTERFACE_H
#define SERIALPORTINTERFACE_H

#include <QByteArray>
#include <QSerialPort>
#include <QTextStream>
#include <QTimer>
#include <QCoreApplication>
#include <QDebug>
#include <QTime>


class SerialPortInterface : public QObject
{
    Q_OBJECT
private:
    QSerialPort *m_serialPort = nullptr;
    QByteArray m_readData;
    QByteArray m_writeData;
    qint64 m_bytesWritten = 0;
    QTimer m_timer;
public:
    explicit SerialPortInterface(QSerialPort *port, QObject *parent = nullptr) :
        QObject(parent)
      , m_serialPort(port)
    {
        connect(m_serialPort, &QSerialPort::readyRead, this, &SerialPortInterface::handleReadyRead);
        connect(m_serialPort, &QSerialPort::errorOccurred, this, &SerialPortInterface::handleError);
   /////////
        m_timer.setSingleShot(true);
        connect(m_serialPort, &QSerialPort::bytesWritten, this, &SerialPortInterface::handleBytesWritten);
        connect(m_serialPort, &QSerialPort::errorOccurred, this, &SerialPortInterface::handleError);
        connect(&m_timer, &QTimer::timeout, this, &SerialPortInterface::handleTimeout);
    }

    void delayMs( int millisecondsToWait ){
        QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
        while( QTime::currentTime() < dieTime )
        {
            QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
        }
    }

    void setParams(QString path){
        if(m_serialPort->isOpen()){
            m_serialPort->close();
        }
        m_serialPort->setPortName(path);
        if (!m_serialPort->open(QIODevice::ReadWrite)) {
            qDebug() <<"Cannot connect to port! ";
            assert(false);
        }
    }

    bool hasCommand(){
        for(auto &data : m_readData){
            if(data=='\r') return true;
        }
        return false;
    }

    void clear(){
        m_readData.clear();
    }

    QString getCommand(){
        QString out ="";
        while(m_readData[0]!='\r'){
            out.append(m_readData.at(0));
            m_readData.remove(0,1);
        }
        m_readData.remove(0,2);
        return out;
    }

    void write(const QByteArray &writeData)
    {
        m_writeData = writeData;

        const qint64 bytesWritten = m_serialPort->write(writeData);

        if (bytesWritten == -1) {
            qDebug() << QObject::tr("Failed to write the data to port %1, error: %2")
                                .arg(m_serialPort->portName())
                                .arg(m_serialPort->errorString())
                             << "\n";
            QCoreApplication::exit(1);
        } else if (bytesWritten != m_writeData.size()) {
            qDebug()  << QObject::tr("Failed to write all the data to port %1, error: %2")
                                .arg(m_serialPort->portName())
                                .arg(m_serialPort->errorString())
                             << "\n";
            QCoreApplication::exit(1);
        }
    }

private slots:
    void handleReadyRead()
    {
        auto data = m_serialPort->readAll();
        m_readData.append(data);
    }

    void handleError(QSerialPort::SerialPortError error)
    {
        if (error == QSerialPort::ReadError) {
            qDebug() << QObject::tr("An I/O error occurred while reading "
                                            "the data from port %1, error: %2")
                                .arg(m_serialPort->portName())
                                .arg(m_serialPort->errorString())
                             << "\n";
            QCoreApplication::exit(1);
        }
        if (error == QSerialPort::WriteError) {
            qDebug()  << QObject::tr("An I/O error occurred while writing"
                                            " the data to port %1, error: %2")
                                .arg(m_serialPort->portName())
                                .arg(m_serialPort->errorString())
                             << "\n";
            QCoreApplication::exit(1);
        }
    }


    /////////////
    void handleBytesWritten(qint64 bytes)
    {
        m_bytesWritten += bytes;
        if (m_bytesWritten == m_writeData.size()) {
            m_bytesWritten = 0;
        }
    }

    void handleTimeout()
    {
        qDebug()  << QObject::tr("Operation timed out for port %1, error: %2")
                            .arg(m_serialPort->portName())
                            .arg(m_serialPort->errorString())
                         << "\n";
    }
};

#endif // SERIALPORTINTERFACE_H
