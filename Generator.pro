#-------------------------------------------------
#
# Project created by QtCreator 2020-11-20T12:48:38
#
#-------------------------------------------------

QT       += core gui serialport charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Generator
TEMPLATE = app


SOURCES += main.cpp

HEADERS  += mainwindow.h \
    engine.h \
    serialportinterface.h

FORMS    += mainwindow.ui
