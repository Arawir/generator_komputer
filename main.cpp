#include "mainwindow.h"
#include <QApplication>

#include "serialportinterface.h"
#include <QCoreApplication>
#include <QSerialPort>
#include <QStringList>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QSerialPort serialPort;
    serialPort.setBaudRate(115200);
    SerialPortInterface serialPortReader(&serialPort);

    QApplication a(argc, argv);
    MainWindow w(&serialPortReader);
    w.show();


    return a.exec();
}
