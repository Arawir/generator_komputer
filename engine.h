#ifndef ENGINE
#define ENGINE

#include <QCheckBox>
#include <QTime>
#include <QTextEdit>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

#include "serialportinterface.h"

QT_CHARTS_USE_NAMESPACE


class MainLayout : public QWidget
{
private:
    QVBoxLayout *layout1;

    QLabel *feedbackLabel;
    QLabel *typeLabel;
    QLabel *freqLabel;
    QLabel *amplLabel;
    QLabel *offsLabel;
    QLabel *pathLabel;


    QTextEdit *feedbackLine;
    QComboBox *typeLine;
    QLineEdit *freqLine;
    QLineEdit *amplLine;
    QLineEdit *offsLine;
    QLineEdit *pathLine;

    QPushButton *startButton;
    QPushButton *adcStartButton;
    QPushButton *connectButton;

    SerialPortInterface *comm;
    QTimer *timer;

    QLineSeries *adc;

    QChart *chart;
    QChartView *chartView;

public:
    explicit MainLayout(SerialPortInterface *newComm){
        comm = newComm;
        layout1 = new QVBoxLayout{};
        setLayout(layout1);
        //////
        feedbackLabel = new QLabel{"Feedback"};
        layout1->addWidget(feedbackLabel);

        feedbackLine = new QTextEdit{""};
        feedbackLine->setReadOnly(true);

        QFontMetrics m = (QFontMetrics)feedbackLine->font();
        int RowHeight = m.lineSpacing();
        feedbackLine->setFixedHeight(6.5*RowHeight);

        layout1->addWidget(feedbackLine);

        //////
        typeLabel = new QLabel{"Signal type"};
        layout1->addWidget(typeLabel);

        typeLine= new QComboBox{};
        typeLine->addItem("Const");
        typeLine->addItem("Sin");
        typeLine->addItem("Tri");
        typeLine->addItem("Rect");
        layout1->addWidget(typeLine);
        //////////////////
        freqLabel = new QLabel{"Frequency (x.xx)[Hz]"};
        layout1->addWidget(freqLabel);

        freqLine = new QLineEdit{"1.00"};
        layout1->addWidget(freqLine);
        ////////////////////
        amplLabel = new QLabel{"Amplitude (x.xx)[V]"};
        layout1->addWidget(amplLabel);

        amplLine = new QLineEdit{"1.00"};
        layout1->addWidget(amplLine);
        ////////////////////
        offsLabel = new QLabel{"Offet (x.xx)[V]"};
        layout1->addWidget(offsLabel);

        offsLine = new QLineEdit{"1.00"};
        layout1->addWidget(offsLine);
        /////////////////////
        pathLabel = new QLabel{"Full path to serial port"};
        layout1->addWidget(pathLabel);

        pathLine = new QLineEdit{"/dev/ttyACM0"};
        layout1->addWidget(pathLine);

        /////////////////////
        connectButton = new QPushButton{"Connect"};
        connect(connectButton, &QPushButton::pressed, this, &MainLayout::reconnect);
        layout1->addWidget(connectButton);

        startButton = new QPushButton{"Update generator"};
        connect(startButton, &QPushButton::pressed, this, &MainLayout::update);
        layout1->addWidget(startButton);

        adcStartButton = new QPushButton{"Start adc"};
        connect(adcStartButton, &QPushButton::pressed, this, &MainLayout::startAdc);
        layout1->addWidget(adcStartButton);

        ////////////////////
        adc = new QLineSeries();


        chart = new QChart();
        chart->legend()->hide();
        chart->addSeries(adc);
        chart->createDefaultAxes();
        chart->setTitle("ADC [V]");
        chart->axisX()->setTitleText("Time [s]");

        chartView = new QChartView(chart);
        chartView->setRenderHint(QPainter::Antialiasing);
        layout1->addWidget(chartView);

    }

    void delayMs( int millisecondsToWait ){
        QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
        while( QTime::currentTime() < dieTime )
        {
            QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
        }
    }



private slots:
    void update(){
        QString command = "sig=" + QString::number(typeLine->currentIndex())+"\r";
        comm->write(command.toStdString().c_str());
        delayMs(400);

        command = "ampl=" + amplLine->text()+"\r";
        comm->write(command.toStdString().c_str());
        delayMs(400);

        command = "freq=" + freqLine->text()+"\r";
        comm->write(command.toStdString().c_str());
        delayMs(400);

        command = "offs=" + offsLine->text()+"\r";
        comm->write(command.toStdString().c_str());
        delayMs(400);
        comm->clear();


        command = "info\r";
        comm->write(command.toStdString().c_str());
        delayMs(400);
        QString feedbackText = "";
        while(comm->hasCommand()){
            feedbackText.append( comm->getCommand() );
            feedbackText.append( "\r\n" );
        }

        feedbackLine->setText(feedbackText);
    }

    void reconnect(){
        comm->setParams(pathLine->text());
        delayMs(500);
    }

    void startAdc(){
        comm->clear();
        comm->write("adc\r");
        delayMs(500);
        adc->clear();
        while(!comm->getCommand().contains("ADC on"));

        for(int i=0; i<11; i++){
            delayMs(500);
            while(comm->hasCommand()){
                QString command = comm->getCommand();
                QString tmpTime;
                QString tmpVal;
                tmpTime.append(command[1]);
                tmpTime.append(command[2]);
                tmpTime.append(command[3]);
                tmpTime.append(command[4]);

                tmpVal.append(command[6]);
                tmpVal.append(command[7]);
                tmpVal.append(command[8]);
                tmpVal.append(command[9]);

                adc->append(tmpTime.toDouble(), tmpVal.toDouble());
            }
            customChartUpdate();
        }
    }

    void customChartUpdate(){
        double minY = findVal(adc,"y","min");
        double minX = findVal(adc,"x","min");
        double maxY = findVal(adc,"y","max")*1.05;
        double maxX = findVal(adc,"x","max");

        chart->axisY()->setRange(minY,maxY);
        chart->axisX()->setRange(minX,maxX);

        chart->update();
    }

    double findVal(QLineSeries *data, QString xy, QString minmax){
        double out;
        if(xy=="x"){
            out = data->at(0).x();
            for(int i=0; i<data->count(); i++){
                if(minmax=="min"){ if(data->at(i).x() < out){ out = data->at(i).x(); } }
                if(minmax=="max"){ if(data->at(i).x() > out){ out = data->at(i).x(); } }
            }
        }
        if(xy=="y"){
            out = data->at(0).y();
            for(int i=0; i<data->count(); i++){
                if(minmax=="min"){ if(data->at(i).y() < out){ out = data->at(i).y(); } }
                if(minmax=="max"){ if(data->at(i).y() > out){ out = data->at(i).y(); } }
            }
        }

        return out;
    }



};


#endif // ENGINE

